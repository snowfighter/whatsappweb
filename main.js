const electron = require('electron');
const path = require('path');
const {
    app,
    BrowserWindow,
    Menu,
    dialog,
    shell,
    Tray
} = electron
const icon = path.join(__dirname, 'img', 'icon.png');
const icon_unread = path.join(__dirname, 'img', 'icon_unread.png');
const wa_regex = /\(\d+\)\s*\w+/g;
const messages_regex = /(\d+)/g;
const URL = 'https://web.whatsapp.com/';
var open_external = false;

app.on('ready', () => {
    const {
        width,
        height
    } = electron.screen.getPrimaryDisplay().workArea;
    app.on('browser-window-created', function (app, BrowserWindow) {
        BrowserWindow.setMenu(null);
    })

    let win = new BrowserWindow({
        width: width / 2,
        height: height / 2,
        icon: icon,
        darkTheme: true,
        frame: true,
        titleBarStyle: "hidden"
    })

    setMainMenu(win)
    var TrayIcon = createTrayIcon(win)
    win.loadURL(URL)

    win.on('minimize', function (event) {
        event.preventDefault()
        win.hide()
    })

    win.on('show', function () {
        TrayIcon.setHighlightMode('always')
    })

    win.on('page-title-updated', () => {
        checkMessages(win, TrayIcon)

    })

    win.webContents.on('new-window', (event, url) => {
        if (open_external) {
            event.preventDefault()
            shell.openExternal(url)
        }

    })

})

app.on('window-all-closed', () => {
    app.quit()
})

// create a custom Menu
function setMainMenu(win) {
    const template = [
        {
            label: 'Options',
            submenu: [
                {
                    label: 'Full Screen',
                    accelerator: 'F11',
                    click() {
                        if (win.isFullScreen()) {
                            win.setFullScreen(false)
                        } else {
                            win.setFullScreen(true)
                        }
                    }
                },
                {
                    label: 'Minimize To Tray',
                    accelerator: 'F12',
                    click() {
                        win.hide()
                    }
                },
                {
                    label: 'Clear Cache...',
                    click() {
                        clearCache(win)
                        win.reload()
                    }
                },
                {
                    label: 'Open URLs in default Browser',
                    type: 'checkbox',
                    checked: open_external,
                    click() {
                        open_external = !open_external;
                    }
                }
            ]
        },

        {
            label: '   WhatsApp - Snow Edition   ',
            click() {
                About()
            }
        }
    ];
    Menu.setApplicationMenu(Menu.buildFromTemplate(template));
}

function About() {
    dialog.showMessageBox({
        title: "About",
        type: "info",
        message: "WhatsApp Web App",
        detail: "v1.0 - by Snow \n\n more stuff coming soon...",
        buttons: ["OK"]
    })

}

function createTrayIcon(win) {
    var TrayIcon = new Tray(icon)
    var contextMenu = Menu.buildFromTemplate([
        {
            label: "Show App",
            click() {
                win.show()
            }
            },
        {
            label: "Quit",
            click() {
                app.isQuiting = true;
                app.quit();
            }
            }
        ])


    TrayIcon.setToolTip('WhatsApp by Sn0w')
    TrayIcon.setContextMenu(contextMenu);
    TrayIcon.on('click', () => {
        win.isVisible() ? win.hide() : win.show()
    })

    return TrayIcon;

}

function checkMessages(win, TrayIcon) {

    var messages = messages_regex.exec(win.webContents.getTitle());

    // TrayIcon.displayBalloon({
    //   icon: icon,
    //   title: "WhatsAppWeb - Sn0w Edition",
    //   content: messages[0] + " ungelesene Nachrichten!"
    // })

    if (messages != undefined) {
        TrayIcon.setImage(icon_unread);
        TrayIcon.setToolTip(messages[0] + " ungelesene Nachrichten!")

    } else {
        TrayIcon.setImage(icon)
        TrayIcon.setToolTip('WhatsApp by Snow')
    }
}

function clearCache(win) {
    win.webContents.session.clearStorageData()
}
