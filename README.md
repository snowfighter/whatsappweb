## WhatsAppWeb port for Win7 / Linux ##

### How to build ###
* make sure Node.js and npm is properly installed
* clone the repository:

```
#!git

git clone https://bitbucket.org/snowfighter/whatsappweb.git
```

```
#!

cd whatsappweb
```
* npm install


### Then you can run the application using ###

```
#!

npm start
```
If you want to build the application run:
* 
```
#!
npm run build-linux

```
for Linux

or


```
#!

npm run build-win
```
for Windows-x64